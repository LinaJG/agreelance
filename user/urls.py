from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', auth_views.LoginView.as_view(template_name='user/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('signup/', views.signup, name='signup'),
    path('change-password/', auth_views.PasswordChangeView.as_view(template_name='user/change-password.html'), name='password'),
    path('change-password/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
]
