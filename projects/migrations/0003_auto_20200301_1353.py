# Generated by Django 2.1.7 on 2020-03-01 12:53

import datetime
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0002_auto_20200116_1130'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='expiration_date',
            field=models.DateField(default=datetime.date(1999, 7, 15)),
        ),
        migrations.AddField(
            model_name='project',
            name='location',
            field=models.CharField(default=django.utils.timezone.now, max_length=200),
            preserve_default=False,
        ),
    ]
