from django.test import TransactionTestCase
from django.test import Client
from .models import Project, ProjectCategory
from .forms import TaskOfferForm, ProjectForm
from django.urls import reverse
from user.models import Profile
from django.contrib.auth.models import User
import datetime
from django.utils import timezone
from django.test import TestCase
import json


# Create your tests here.

class TestViews(TestCase):
    
    def setUp(self):

        self.client = Client()
        user = User.objects.create(username='nombre', password='1234556')
        profile =user.profile
        test_expiration_date = datetime.date(year=2020, month=10,day=10)
        self.project = Project.objects.create(
            user = profile,
            title = 'title',
            description = 'description',
            category = ProjectCategory.objects.create(name = 'cleaning'),
            location = 'location',
            expiration_date = datetime.date(year=2020, month=10,day=10),
            added_date = timezone.now()
        )
        self.project_url = reverse('project_view', args=[self.project.pk])
  
    def test_project_view_POST(self):
        
        category = ProjectCategory.objects.get_or_create(name='cleaning')
        response = self.client.get(self.project_url, {
            'title':'title',
            'description':'description',
            'category': category,
            'location':'location',
            'expiration_date':datetime.date(year=2020, month=10,day=10),
            
            })
        response_project_title = response.context.get('project')
        self.assertEqual(response.status_code, 200)
        self.assertEquals(response_project_title, self.project)
        

        
# Create your tests here.
class test_project_offer(TestCase):
    def test_project_offer_price_field(self):
        for a in range (0, 110000, 100):
            form = TaskOfferForm(data = {'title' : 'Titel', 'description' :'Description', 'price':a})
            self.assertTrue(form.is_valid())


class test_new_function_expiration_date(TestCase):
    def setUp(self):
        self.category = ProjectCategory.objects.create(id=1, name='category1')

    # Expiration Date Tests
    def test_new_project_expiration_date_FUTURE(self):
        test_date = datetime.date.today()+datetime.timedelta(days=1) 
        form = ProjectForm(data = {'title':'Test', 'description':'Description', 'category_id':1, 'location':'Trondheim', 'expiration_date':test_date})
        self.assertTrue(form.is_valid())

    def test_new_project_expiration_date_PAST(self): 
        test_date = datetime.date.today()-datetime.timedelta(days=1) 
        form = ProjectForm(data = {'title':'Test', 'description':'Description', 'category_id':1, 'location':'Trondheim', 'expiration_date':test_date})
        self.assertFalse(form.is_valid())  

    def test_new_project_expiration_date_CURRENT(self):
        test_date = datetime.date.today()
        form = ProjectForm(data = {'title':'Test', 'description':'Description', 'category_id':1, 'location':'Trondheim', 'expiration_date':test_date})
        self.assertFalse(form.is_valid())

class test_new_function_added_date(TestCase):
    def setUp(self):
        self.client = Client()
        test_expiration_date = datetime.date.today()+datetime.timedelta(days=1)
        user = User.objects.create(username ='test', password = 'test1234')
        category = ProjectCategory.objects.create(id=1, name='category1')
        profile = user.profile
        self.project = Project.objects.create(user = profile, 
                                         title ='title', 
                                         description = 'description',
                                         category = category,
                                         location = 'location', 
                                         expiration_date = test_expiration_date,
                                         status = 'o')

    def test_added_date_is_today(self):
        date_today = datetime.date.today()
        added_date= self.project.added_date.date()
        self.assertEqual(added_date, date_today)


class test_new_function_filter(TestCase):
    def setUp(self):
        self.client = Client()
        test_expiration_date = datetime.date.today()+datetime.timedelta(days=1)
        user = User.objects.create(username ='test', password = 'test1234')
        category = ProjectCategory.objects.create(id=1, name='category1')
        profile = user.profile
        project = Project.objects.create(user = profile, 
                                         title ='title', 
                                         description = 'description',  
                                         category = category,
                                         location = 'location', 
                                         expiration_date = test_expiration_date,
                                         status = 'o')
       
    def test_filter_url_exists_at_desired_location(self):
       response = self.client.get('/projects/search')
       self.assertEqual(response.status_code, 200)

    def test_filter_url_accessible_by_name(self):
        response = self.client.get(reverse('search'))
        self.assertEqual(response.status_code, 200)
        
    def test_filter_uses_correct_template(self):
        response = self.client.get(reverse('search'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'projects/project_search.html')